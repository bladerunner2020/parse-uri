var parseUri = require('./index').parseUri;
parseUri.options.strictMode = true;
var http = require('http');

http.createServer(function (req, res) {
    var url = parseUri(req.url);

    var cmd = {};
    cmd.name = url.path.replace(/^\/|\/$/g, '');
    cmd.firstDate = url.queryKey.firstDate;
    cmd.email = url.queryKey ? url.queryKey.email : '';

    console.log(cmd);
    
    res.writeHead(200, {'Content-Type': 'text/html '});
    res.write('<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">');
    res.write('Email = ' + cmd.email);
    res.end();
}).listen(8080);


var s = '/getCalendar?id=cnf.8.02@auvix.ru';

var uri = parseUri(s);
console.log(uri);